PARSE_CON = r'(?!(\"|\'|\'\'\'|\"\"\"|#))'

# ----if------
IF_START_RE = r"(elif|if)\((.*)\){"
IF_START_PYTHON = "%if%(%logic%):"
IF_START_CPP = "%if%(%logic%){"
# ----else----
ELSE_RE = r"else{"
ELSE_PYTHON = "else:"
ELSE_CPP = "else{"
# ----end-----
END_RE = r"}"
END_PYTHON = ""
END_CPP = "}"
# ----for-----
FOR_START_RE = r"for\((.*)=(.*) to (.*) step=(.*)\){"
FOR_START_PYTHON = "for %var% in range(%start%,%end%,%step%):"
FOR_START_CPP = "for(%var%=%start%;%var%<%end%;%var%++){"
# ----logic operators----
LOGICS = ["and", "or", "not"]
LOGICS_PYTHON = LOGICS
LOGICS_CPP = ["&&", "||", "!"]
LOGICS_RE = fr"{PARSE_CON}and|or|not{PARSE_CON}"
# ------lib import-------
IMPORT_RE=fr"import (.*)"