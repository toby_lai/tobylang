# tobylang

------

一个基于python的编程语言，可翻译自身至python和c++




## 安装教程

可到发行版获取打包出来的文件
[https://gitee.com/toby_lai/tobylang/releases](https://gitee.com/toby_lai/tobylang/releases)

安装指南：[wiki：安装Tobylang](https://gitee.com/toby_lai/tobylang/wikis/pages?sort_id=4841815&doc_id=1790804)

如果需要最新版本，请clone本仓库
并把示例的所有`tobylang`替换成`python tobylang.py`
**以下内容为最新版本1.3.0+，发行版未必有某些功能**

## 命令行工具

```bash
$ tobylang --help
Usage: tobylang [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  compile    compile tobylang files (*.toby) [reqiure g++ compiler]
  logo       show the ascii logo of tobylang [beta]
  run        run tobylang files (*.toby) [using inside python interpreter]
  translate  translate tobylang file(*.toby) to other languages(defualt c++)
  version    show the version of tobylang
```
#### 若不输入任何命令，你可以进入tobylang交互式命令行/笔记本

**1.2.0正式版 无if/else多行语句，所以无法多行输入，且没有补全、笔记本和样式**

**图片为1.2.2Pre版本的交互式笔记本**

tobylang命令行带有多行输入（esc+enter运行）和自动补全的功能
### 操作指南
1. 单独输入`exit`来退出
2. 单独输入`:nsave`来保存为笔记本（会询问保存位置，目前无法保存结果输出数据，但是可以储存变量）
3. 单独输入`:nread`来读取笔记本（会询问读取位置）

![tobylang交互式笔记本](https://images.gitee.com/uploads/images/2021/1114/085918_92955880_6580637.png "屏幕截图.png")

1. compile 编译

   编译成可执行文件

   ```bash
   tobylang compile
   --file -f 需要编译的tobylang文件
   --out -o  编译输出的可执行文件（类似g++ -o）
   --encoding -e 文件编码(utf-8)
   --silent -s 编译过程有输出信息，加上来屏蔽错误信息以外的输出
   --compiler -c 使用的编译器，使用GNU-C++的编译器或交叉编译器 默认值为g++
   ```

2. run 运行

   直接运行

   ```bash
   tobylang run
   --file -f 需要运行的文件
   --encoding -e 文件编码(utf-8)
   ```

3. translate 翻译

   可以翻译成其他编程语言

   ```bash
   tobylang translate
   --file -f 文件
   --encoding -e 编码
   --language -l 编程语言 c++|python3|py3|cpp
   ```

4. version 版本

   ```bash
   $ tobylang version
   tobylang v1.2.0 (Windows-10-10.0.19041-SP0)
   ```

5. logo (beta)
   
   **需求 1.2.2Pre+**
   
   show the ascii logo of tobylang
   ```bash
   $ tobylang logo
   ```
   <img src="https://images.gitee.com/uploads/images/2021/1114/074905_64ec7f69_6580637.png" alt="命令行输出的logo长这样" title="屏幕截图.png" style="zoom:45%;" />

# Vscode 语法高亮插件安装
在vscode插件搜索栏中搜索：`tobylang` 并安装

此插件会关联所有`(*.toby)`类型的tobylang文件
![tobylang](https://images.gitee.com/uploads/images/2021/1120/232326_9cfbeda1_6580637.png "屏幕截图.png")

建议定期查看更新，高亮插件会定期跟上发行版进度**某些还未列入正式版的功能不会高亮**

储存库：[tobylang-vscode-highlighting](https://gitee.com/toby_lai/tobylang-vscode-highlighting) **发行版`标题/内容`会标明对应的`tobylang`版本**

## 语法入门

### 类型

```c++
int 整数(c++ int,python3 int) 形式：整数                              例子：10,-10,123,256,-100

long 长整型(c++ long long,python3 int) 形式：整数LL                   例子：10LL,-10LL,12388888LL,-100000LL

string 字符串(c++ string/char[],python3 str) 形式："内容"、'内容'      例子："hello","hello world",'hi','hello hi'

double 浮点数(c++ double,python float)形式：小数    需求 1.2.2Pre+     例子：1.0,-1.111,2.123

bool 布尔值(c++ bool,python bool)形式：true/false   需求 1.3.1Pre+     例子：true,false,(1,0也适用）
                                                                              
vector<类型> 数组(c++ deque<type>,python list)     需求 1.3.9Pre+     声明：create vector<类型>:变量名
```

### 变量声明

声明不支持赋值，请另外赋值

```c++
create 类型:变量名
例子：
create int:hello
create string:abc
create long:h123
create double:ohhdouble
create bool:flag
create vector<int>:v    创建一个包含int类型的数组v [v1.3.9Pre+]
```

### 变量赋值

```c++
对于已创建的变量，可以这样赋值：
set 变量名=值
set hello=1
set abc="tobylang"
set h123=222222LL
set ohhdouble=1.0+2
set flag=true
```
### 函数调用

和大多数编程语言一样

```c++
函数名(参数列表，支持变量，用英文逗号隔开)
set name=stringinput("what's your name? ")
print("hello world")
print("hi",name)
```

### 内置函数

目前很少

详细介绍：[wiki：函数](https://gitee.com/toby_lai/tobylang/wikis/pages?sort_id=4841820&doc_id=1790804)

| 函数名             | 参数        | 返回值       | 例子                                   | 作用                                |
| :----------------- | :---------- | ------------ | -------------------------------------- | ----------------------------------- |
| `print`            | a,b,c,d.... | 无           | `print("hello","world")`               | 输出内容，空格分割参数列表          |
| `stringinput`      | msg         | string       | `set name=stringinput("Who are you?")` | 输出msg，然后询问输入，再把输入返回 |
| `strlen/stringlen` | text        | int          | `strlen("hello") #返回长度`            | 获取字符串长度                      |
| `push_back`        | v,x         | 无           | `push_back(v,1)`                       | 给数组`v`末尾插入`x`                |
| `push_front`        | v,x         | 无           | `push_front(v,1)`                       | 给数组`v`开头插入`x`                |
| `pop_back`         | v           | 无           | `pop_back(v)`                          | 删除数组`v`的末尾                   |
| `get`              | v,i         | 索引对应的值 | `get(v,0)`                             | 获取数组`v`索引`i`的值              |
| `set`              | v,i,x       | 无           | `set(v,0,x)`                           | 设置数组`v`索引`i`的值为`x`         |
| `erase`              | v,i         | 无 | `erase(v,0)`                             |  删除数组`v`索引`i`项  |


### if/elif/else 逻辑判断
**需求 1.2.2Pre+**
与c++和python的差不多

1.2.2Pre~1.3.0 结尾花括号必须单独占一行

1.4.0+ 结尾花括号将支持花括号后面添代码 (v1.4.0+准备功能，请使用tobylang.py运行代码体验)
```c++
if(...){
   print("yes")
}
elif(...){
    .......
}
else{
   print("no")
}
```

### for 循环

**需求 1.3.0+**

For循环，和if一样，**尾花括号要单独占一行**

实际遍历的是`初始值~结束值-1 一步加step`

格式如下：

```c++
// 变量需要先声明(c++模式)
create int:变量
for(变量=初始值 to 结束值 step=一步加多少){
    要做的内容
}
// 例子
create int:i
for(i=0 to 5 step=1){
    print(i)
}
//0
//1
//2
//3
//4
```

### 导入库

**v1.4.0+准备功能，请使用tobylang.py运行代码体验**

使用 `import 库名`来导入一个新的库（自带库/第三方库），会将该库的所有内容引入到程序里

库导入位置：

1. `tobylang可执行文件目录/lib` 第三方库
2. `tobylang可执行文件目录/stdlib` 内置库
3.  `当前位置` 当前位置搜索库，有config.yml且匹配名字的文件夹会被选中（请事先做好准备）
3.  默认已导入：`stdlib/__builtin__vector_utils__` vector数组操作

搜索顺序`内置库->当前位置->第三方库`

```python
import random
create int:var
setuprand()
set var=randint(1,2)
print(var) # 会输出1~2之间的随机数
```

### 编写库

**v1.4.0+准备功能，请使用tobylang.py运行代码体验**

打开`tobylang可执行文件目录/lib` 目录（第三方库）

创建一个名为`你创建库的名字`的文件夹，这里取名helloworld

![helloworld tobylang lib](https://images.gitee.com/uploads/images/2021/1120/151431_75af9333_6580637.png "tobylang")

在**该文件夹下**创建一个文件名为`config.yml`的文件

![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/151700_2dfae672_6580637.png "屏幕截图.png")

按照提示编写内容**务必保留键值对，否则导入时会报错**：

```yaml
# cpp 添加内容(函数，让接下来代码可以调用，通常功能多行时要这么做)
cpp_exts: 
  hello_name: 'void helloname(string a){cout<<"hello "<<a<<endl;}'
# cpp 导入库
cpp_includes:
  - cstring # 导入c++的哪些库？

#python 添加内容(函数，让接下来代码可以调用，通常功能多行时要这么做,不需要只留{})
python_exts: {}

#python 导入库 不需要，只留[]
python_includes: []

#tobylang函数定义
functions: 
  hello: # 接下来的函数也是这样定义，注意缩进
    args: ["name"] #参数列表，逗号隔开
    # 转译代码 $name对应args里的"name"
    python: 'print("hello",$name)' # 翻译成python的代码
    cpp: "helloname($name)"  # 翻译成c++的代码
```

然后你就可以在程序里调用：

```python
import helloworld
hello("tobylang")

# 结果：
#hello tobylang
```

### 自带库列表

详见 [Wiki：自带库一览](https://gitee.com/toby_lai/tobylang/wikis/pages?sort_id=4860345&doc_id=1790804)

### 转义

由于tobylang机制问题，字符串内写关键字、符号等最好转义，否则会有意想不到的结果
转义符：|||（三竖线）
用法：隔开关键字（如果有问题的话），输出隐藏

### 更多
[更多内容详见wiki](https://gitee.com/toby_lai/tobylang/wikis/pages)

## 编译tobylang

如果你想编译tobylang源码的话，请看此处

```bash
git clone https://gitee.com/toby_lai/tobylang.git # 国内gitee，很快
cd tobylang
pip3 install -r requirements.txt # 安装必要依赖，包括了打包工具
pyinstaller -F tobylang.py # 一般打包
```

如果想要加上图标，请执行：

```bash
pyinstaller -F tobylang.py -i icons/tobylang.ico
```

如果你想要一个虚拟环境来打包，请再执行上面**所有**操作之前按步骤执行：

```bash
# 如果你想创建一个虚拟环境来打包（通常你装了许多第三方包的时候你需要这么做）
# 不需要则忽略 ↓
pip3 install virtualenv
virtualenv venv
# 请选择自己的终端
.\venv\Scripts\activate.bat # cmd
.\venv\Scripts\activate.ps1 # powershell
./venv/Scripts/activate # bash
./venv/Scripts/activate.fish # fish
# 不需要则忽略 ↑
```

## 写在最后
放一张tobylang的字符画~
```python
                     %%%%%%            %%
                  ....%%%%%%%%      %%%%%%%%
████████         ██%%%%%%%%%%      ██%%%%%%%%
   ██            ██%%%%%%%%%%%%  %%██%%%%%%%%
   ██    ██████  ████████%%██%%██%%██%%██████  ████████  ████████  
   ██    ██  ██  ██%%%%██%%██%%██%%██%%██%%██  ██    ██  ██    ██
   ██    ██  ██  ██  %%██%%██%%██%%██%%██%%██  ██    ██  ██    ██  
   ██    ██████%%████████%%██████%%██%%██████  ██    ██  ████████  
         %%%%%%%%%%    %%%%....██    %%%%%%██  %%%%%%          ██
         %%%%%%%%%%%%      ██████          %%%%%%%%%%    ████████  
         %%%%%%%%%%%%                    %%%%%%%%%%%%
         %%%%%%%%%%%%      ....%%%%        %%%%%%%%%%%%
            %%%%%%%%%%    %%%%%%%%%%%%    %%%%%%%%%%%%
            %%%%%%    %%%%%%%%%%%%%%%%  %%%%%%%%%%
                     %%%%%%%%%%%%%%%%%%%%%%%%%%%%....
                  %%%%%%%%%%%%%%%%%%%%%%  %%%%
                  %%%%%%%%%%%%%%%%%%%%%%%%
                  %%%%%%%%%%%%%%%%%%%%%%%%%%
                  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
               %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                  ....%%%%%%%%        %%%%%%....
```

```issue pr star fork 刷起来——```

