# Builtin Types
# int,long,string,double,operate(not usable - will act like int but it's needed)
# bool(1048576's upd)
# Todo:
#  - Varibles       - done [tobylai]
#  - Operator       - done [tobylai]
#  - Double         - done [tobylai]
#  - Boolean        - done? [1048576]
#  - string to c++  - doing [tobylai]
import re
import fnmatch

BUILTIN_OPERATORS = ["+", "-", "*", "/", "%"]
RE_OPERATORS = ["+", ".", "|", "*"]
PARSE_CON = '(?!(\"|\'|\'\'\'|\"\"\"|#))'


class BaseType(object):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        return True

    def to_cpp(self, text):
        return ""

    def to_python3(self, text):
        return ""


class Int(BaseType):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        if re.search(r"(-?\d+)", text) and re.search(r"(-?\d+)", text).group(0) == text:
            return True
        return False

    def to_cpp(self, text):
        return text

    def to_python3(self, text):
        return text


class Double(BaseType):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        if re.search(r"(-?\d+\.\d+)", text) and re.search(r"(-?\d+\.\d+)", text).group(0) == text:
            return True
        return False

    def to_cpp(self, text):
        return text

    def to_python3(self, text):
        return text


class Var(BaseType):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        return False

    def to_cpp(self, text):
        return text

    def to_python3(self, text):
        return text


class Long(Int):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        if re.search(r"(-?\d+LL)", text) and re.search(r"(-?\d+LL)", text).group(0) == text:
            return True
        return False

    def to_cpp(self, text):
        return text + "LL"


class String(BaseType):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        pat1 = "(%s)" % fnmatch.translate('"*"')
        pat2 = "(%s)" % fnmatch.translate("'*'")
        if ((re.search(pat1, text) and re.search(pat1, text).group(0) == text) or (
                re.search(pat2, text) and re.search(pat2, text).group(0) == text)):
            return True
        return False

    def to_cpp(self, text):
        pat1 = "(%s)" % fnmatch.translate('"*"')
        pat2 = "(%s)" % fnmatch.translate("'*'")
        if re.search(pat1, text) and re.search(pat1, text).group(0) == text:
            return "((string)%s)" % text
        elif re.search(pat2, text) and re.search(pat2, text).group(0) == text:
            inner = list(re.search(pat2, text).group(0))
            inner[0] = ""
            inner[-1] = ""
            inner = "".join(inner)
            # print('((string)"%s")' % inner.replace("|||", ""))
            return '((string)"%s")' % inner
        # print("((string)%s)"%text.replace("|||", ""))
        return "((string)%s)" % text

    def to_python3(self, text):
        # return text.replace("|||", "")
        return text


class Operate(BaseType):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        re_op_text = ""
        a = []
        for i in BUILTIN_OPERATORS:
            if (i in RE_OPERATORS):
                a.append("\\" + i)
            else:
                a.append(i)
        re_op_text = "|".join(a)
        if re.match(fr"{PARSE_CON}(.*({re_op_text}).*)+{PARSE_CON}", text):
            return True
        return False

    def to_cpp(self, text):
        return text

    def to_python3(self, text):
        return text


class Boolean(Int):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        if re.match(fr"{PARSE_CON}(true|false){PARSE_CON}",text):
            return True
        return False

    def to_cpp(self, text):
        return text

    def to_python3(self, text):
        return text.capitalize()

class Vector(Int):
    def __init__(self, value="") -> None:
        super().__init__()
        self.value = value

    def is_match(self, text) -> bool:
        if re.match(PARSE_CON + r"\{.*\}" + PARSE_CON,text):
            return True
        return False

    def to_cpp(self, text):
        return text

    def to_python3(self, text):
        return text

# class Varible(BaseType):
#     def __init__(self, value="") -> None:
#         super().__init__()
#         self.value = value

#     def is_match(self, text):
#         if re.match("^[a-zA-Z_][a-zA-Z0-9_]", text.strip())==text.strip():
#             return True
#         return False

#     def to_cpp(self, text):
#         return text

#     def to_python3(self, text):
#         return text
BUILTIN_DTYPES={"vector":"vector<%type%>"}
BUILTIN_DTYPES_TO_PY={"vector":"[]","string":'""',"int":"0","double":"0.0","long":"0","bool":"False"}
BUILTIN_TYPES = {"int": Int, "long": Long, "string": String, "double": Double, "operate": Operate, "bool": Boolean,"vector<%type%>":Vector}
BUILTIN_TYPES_TO_CPP = {"int": "int", "long": "long long", "string": "string", "double": "double", "operate": "int", "bool" : "bool","vector<%type%>":"deque<%type%>"}

def match_type(text:str):
    for i in BUILTIN_TYPES:
        tempType = BUILTIN_TYPES[i]()
        if tempType.is_match(text):
            return i
        del tempType
    # print(text)
    if (re.match(fr"(^[a-zA-Z_][a-zA-Z0-9_]*)", text.strip()) and re.match(fr"(^[a-zA-Z_][a-zA-Z0-9_]*)", text.strip()).group(1)==text.strip()) and not re.match("(true|false)",text):
        return "var"
    # elif(re.match(r".*(\+|\-|\*||/|\%).*",text)):
    #     return "operate"
    return 0
