import interactive_console
import os
import subprocess

import click
from colorama import Fore, init

import lang
import sys,os

curdir=os.path.dirname(os.path.abspath(__file__))
init(autoreset=True)

import os

class ExpandedPath(click.Path):
    def convert(self, value, *args, **kwargs):
        value = os.path.expanduser(value)
        return super(ExpandedPath, self).convert(value, *args, **kwargs)

def error(msg, exit_=True):
    print("{}error:{}".format(Fore.RED, msg))
    if exit_:
        sys.exit(1)


def detect_gpp(exe):
    p = subprocess.Popen(f"{exe} -v".split(" "), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return p.wait()==0

if(not os.path.exists(os.path.join(curdir,"lib"))):
    os.makedirs(os.path.join(curdir,"lib"),exist_ok=True)

if (len(sys.argv) == 1):
    interactive_console.start_console()


@click.group()
def group_run():
    pass


@click.command(help="compile tobylang files (*.toby)[using GNUC++Compiler/Cross-Compiler]",short_help="compile tobylang files (*.toby) [using GNU C++ Compiler/Cross-Compiler]")
@click.option("--file", '-f', type=ExpandedPath(exists=True), required=True,
              help="the file you want to compile (usually *.toby)")
@click.option("--out", '-o', type=ExpandedPath(file_okay=True), default="toby", help="compile output file")
@click.option("--encoding", "-e", type=click.STRING, default="utf-8", help="the encoding of the file")
@click.option("--silent", "-s", is_flag=True, help="don't output messages except errors")
@click.option("--compiler", "-c", type=click.STRING, default="g++", help="compiler path/compiler in path you want to use (GNU C++ Compiler,you can choose a GNU C++ cross-compiler too)")
def compile_(file, out, silent, encoding,compiler):  # what the fuck compile is a build-in?
    filename=os.path.basename(file)
    os.chdir(os.path.abspath(os.path.dirname(file)))
    
    if not silent: print(f"reading file {file}...")
    with open(filename, encoding=encoding) as fp:
        code = fp.read()
    comp_cpp_code = lang.cpp(code)
    with open(f"{file}.translated.cpp", "w", encoding="utf-8") as fp:
        fp.write(comp_cpp_code)
    if detect_gpp(compiler):
        if not silent: print(f"using g++ compiler [{compiler}] to compile...")
        p = subprocess.Popen(f"{compiler} {file}.translated.cpp -o {out}".split(" "))
        if not p.wait():
            if not silent: print("compile finished")
            os.remove(f"{file}.translated.cpp")
            sys.exit()
        else:
            error("compile failed")
            os.remove(f"{file}.translated.cpp")
    else:
        error(f"tobylang needs GNU C++ Compiler/Cross-Compiler to compile. Is \"{compiler}\" exists/in path?")

@click.command(help="runs the interactive console/notebook")
def interactive():
    interactive_console.start_console()

@click.command(help="runs tobylang files (*.toby) [using inside python interpreter]")
@click.option("--file", '-f', type=ExpandedPath(exists=True), required=True,
              help="the file you want to run (usually *.toby)")
@click.option("--encoding", "-e", type=click.STRING, default="utf-8", help="the encoding of the file")
def run(file, encoding):
    filename=os.path.basename(file)
    os.chdir(os.path.abspath(os.path.dirname(file)))
    with open(filename, encoding=encoding) as fp:
        code = fp.read()
    comp_py_code = lang.python3(code)
    import pyrun
    pyrun.__TOBYLANG__RUNCODE_EXEC__(comp_py_code)
    del pyrun


@click.command(help="translate tobylang file(*.toby) to other languages(defualt c++)")
@click.option("--file", '-f', type=ExpandedPath(exists=True), required=True,
              help="the file you want to run (usually *.toby)")
@click.option("--encoding", "-e", type=click.STRING, default="utf-8", help="the encoding of the file")
@click.option("--language", "-l", type=click.Choice(["c++", "python3", "py3", "cpp"]), default="c++",
              help="the language you wan to translate to")
def translate(file, encoding, language):
    filename=os.path.basename(file)
    os.chdir(os.path.abspath(os.path.dirname(file)))
    # print(os.getcwd())
    
    langs = {
        "c++": lang.cpp,
        "python3": lang.python3,
        "py3": lang.python3,
        "cpp": lang.cpp
    }
    with open(filename, encoding=encoding) as fp:
        code = fp.read()
    comp_code = langs[language](code)
    print(comp_code)


@click.command(help="show the version of tobylang")
def version():
    import tobylanginfo
    print(f"{tobylanginfo.NAME} v{tobylanginfo.VERSION} ({tobylanginfo.PLATFORM})")


@click.command(help="show the ascii logo of tobylang [beta]")
def logo():
    import tobylanginfo
    ascii_logo = tobylanginfo.LOGO
    print(ascii_logo.replace("$RED--$", Fore.RED).replace("$BLUE-$", Fore.BLUE).replace("$GREEN$", Fore.GREEN).replace(
        "$BLACK$", Fore.LIGHTBLACK_EX))


group_run.add_command(compile_,"compile")
group_run.add_command(run)
group_run.add_command(translate)
group_run.add_command(version)
group_run.add_command(logo)
group_run()
