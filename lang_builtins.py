import sys
import colorama
from builtin_types import *
colorama.init(True)
INTERACTIVE = False
NEEDEDTOEXIT = not INTERACTIVE


class BaseFunction(object):
    def __init__(self) -> None:
        super().__init__()

    def to_python3(self, args):
        return ""

    def tp_cpp(self, args):
        return ""


class PrintFunction(BaseFunction):
    def to_python3(self, args):
        pyarglist = ",".join([(i.to_python3(i.value)) for i in args])
        return "print(%s)" % pyarglist

    def to_cpp(self, args):
        cpp_cout_arg = '<<" "<<'.join(
            [i.to_cpp(i.value) for i in args])+"<<endl"
        return "cout<<%s" % cpp_cout_arg


class StringInputFunction(BaseFunction):
    def to_python3(self, args):
        if(len(args) > 1):
            error("input only takes 0 or 1 arguments", NEEDEDTOEXIT)
        pyarglist = ",".join([(i.to_python3(i.value)) for i in args])
        return "input(%s)" % pyarglist

    def to_cpp(self, args):
        if(len(args) > 1):
            error("input only takes 0 or 1 arguments", NEEDEDTOEXIT)
        cpp_arg = ','.join([i.to_cpp(i.value) for i in args])
        return "stringinput(%s)" % cpp_arg


class StringLenFunction(BaseFunction):
    def to_python3(self, args):
        if(len(args) > 1):
            error("input only takes 0 or 1 arguments", NEEDEDTOEXIT)
        pyarglist = ",".join([(i.to_python3(i.value)) for i in args])
        return "len(%s)" % pyarglist

    def to_cpp(self, args):
        if(len(args) > 1):
            error("input only takes 0 or 1 arguments", NEEDEDTOEXIT)
        cpp_arg = ','.join([i.to_cpp(i.value) for i in args])
        return "%s.size()" % cpp_arg


def get_func_call(text):
    lang_arglist = []
    arglist = []
    funcname, arglist_str = re.match(FUNC_CALL_RE, text).groups()
    arglist = arglist_str.split(",")
    for i in range(len(arglist)):
        try:
            if((arglist[i][0] == "'" and arglist[i][-1] != "'") and (arglist[i+1][0] != "'" and arglist[i+1][-1] == "'")):
                lang_arglist.append(arglist[i]+","+arglist[i+1])
            elif((arglist[i][0] == '"' and arglist[i][-1] != '"') and (arglist[i+1][0] != '"' and arglist[i+1][-1] == '"')):
                lang_arglist.append(arglist[i]+","+arglist[i+1])
            elif(((arglist[i][0] == "'" or arglist[i][0] == '"') and (arglist[i][-1] == "'" or arglist[i][-1] == '"')) or (arglist[i][-1] != "'" and arglist[i][-1] != '"')):
                lang_arglist.append(arglist[i])
        except:
            pass
    # print(lang_arglist)
    builtin_arglist = []
    for i in lang_arglist:
        tp = match_type(i)
        if(tp):
            if(tp != "var"):
                tp = BUILTIN_TYPES[tp](i)
            else:
                tp = Var(i)
        builtin_arglist.append(tp)
    return (funcname, builtin_arglist)


def error(msg, exit_=True):
    print("{}{}".format(colorama.Fore.RED, msg))
    if(exit_):
        sys.exit(1)


BUILTIN_FUNCS = {"print": PrintFunction,
                 "stringinput": StringInputFunction, 
                 "stringlen": StringLenFunction,
                 "strlen": StringLenFunction,
                }
FUNC_CALL_RE = r"(^[a-zA-Z_][a-zA-Z0-9_]*)\((.*)\)"
VARIBLE_CREATE_RE = r"create (.*):(.*)"
VARIBLE_SET_RE = r"set (.*)=(.*)"
