import lang as __lang__
import inspect as __inspect__
def __match_save_type__(pi):
    return type(pi)==int or type(pi)==float or type(pi)==str or type(pi)==bool or type(pi)==list
def __run_code__(code):
    exts = ""
    for i in __lang__.PYTHON_EXTS:
        exts += f"# ext:{i}\n"+__lang__.PYTHON_EXTS[i]+"\n"
    includes = ""
    for i in __lang__.PYTHON_INCLUDES:
        includes += f"import {i}\n"
    try:
        exec(__import__("lang").python3_interactive(code))
    except Exception as e:
        print("error>",str(e))
        # raise e

def __prompt_continuation__(width, line_number, is_soft_wrap):
    return '.' * 3+" "
    # Or: return [('', '.' * width)]


def start_console():
    __run_code__("import __builtin__vector_utils__")
    from prompt_toolkit import prompt, PromptSession
    from prompt_toolkit.formatted_text import ANSI
    from prompt_toolkit.completion import WordCompleter
    from prompt_toolkit.history import InMemoryHistory, FileHistory,History
    from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
    from prompt_toolkit.shortcuts import yes_no_dialog, input_dialog, message_dialog
    __import__("signal").signal(__import__("signal").SIGINT,
                                (lambda a, b: __import__("sys").exit(0)))
    __tobylang__imported__colorama__ = __import__("colorama")
    info_tblang = __import__("tobylanginfo")
    print(
        f"{info_tblang.NAME}({info_tblang.VERSION}) on platform [{info_tblang.PLATFORM}]\ninteractive using python3 core, so create is not needed here")
    c = __tobylang__imported__colorama__
    print(
        f'{c.Fore.GREEN}Press [ESC] then press [Return]  to run current code{c.Fore.RESET}')
    print(f'{c.Fore.CYAN}add option "--help" for help and type "exit" to exit{c.Fore.RESET}')
    os = __import__("os")
    # if(not __import__("os").path.exists(os.path.join(os.path.expanduser('~'), '.tobylanginteractivehistory'))):
    #     with open(os.path.join(os.path.expanduser('~'), '.tobylanginteractivehistory'), "w", encoding="utf-8")as fp:
    #         fp.write("")

    session = PromptSession()

    def bottom_toolbar():
        # prompt toolkit bug - you need to flip Fore and Back
        tobylang = f'{c.Fore.BLUE}{c.Back.BLACK} INTERACTIVE {c.Fore.RESET}{c.Back.RESET}'
        tobylanginfo = f"{c.Fore.YELLOW}{c.Back.BLACK} v{info_tblang.VERSION} {c.Fore.RESET}{c.Back.RESET}"
        platform = f'{c.Fore.MAGENTA}{c.Back.BLACK} {info_tblang.PLATFORM[:8]}... {c.Fore.RESET}{c.Back.RESET}'
        runtip = f'{c.Fore.GREEN}{c.Back.BLACK} run[esc+return] {c.Fore.RESET}{c.Back.RESET}'
        python3core = f'{c.Fore.RED}{c.Back.WHITE} core:py3 {c.Fore.RESET}{c.Back.RESET}'
        nsave = f'{c.Fore.LIGHTBLUE_EX}{c.Back.WHITE} save[:nsave]▕ read[:nread] {c.Fore.RESET}{c.Back.RESET}'
        return ANSI(tobylang+tobylanginfo+platform+runtip+python3core+nsave)
    types = ["int", "double", "string", "long","bool","vector"]
    funcs = ["print", "stringinput","set","get","erase","pop_back","push_back"]
    keywords = ["set", "create", "exit", "if", "elif", "else","for"]
    completer = WordCompleter(sorted(types+funcs+keywords))

    while(True):
        c = __tobylang__imported__colorama__
        code = session.prompt(ANSI(f"{c.Fore.BLUE}>>> {c.Fore.RESET}"),
                              multiline=True, prompt_continuation=__prompt_continuation__,
                              completer=completer, auto_suggest=AutoSuggestFromHistory(),
                              complete_while_typing=True,
                              bottom_toolbar=bottom_toolbar)
        if(code.strip() == "exit"):
            __import__("sys").exit(0)

        elif(code.strip() == ":nsave"):
            confirm = yes_no_dialog(
                title='Confirm', text='Do you want to save this interactive notebook?')
            if(confirm):
                file_location=input_dialog("save notebook","Where do you want to save?\nexample:./a.tnote",)
                if(file_location):
                    try:
                        import pickle
                        local_list=[]
                        locals_=locals()
                        modules=[]
                        for i in locals_:
                            pi=locals_[i]
                            if(__match_save_type__(pi)):
                                local_list.append((i,locals_[i]))
                            elif(__inspect__.ismodule(pi)):
                                if("__" not in i):
                                    modules.append(i)
                        obj={
                            "history":session.history.get_strings(),
                            "locals":local_list,
                            "module":modules
                        }
                        with open(file_location,"wb",) as fp:
                            pickle.dump(obj,fp)
                        print("save success")
                    except Exception as e:
                        print("error:\n",e)
                else:
                    print("canceled")
            else:
                print("canceled")
        elif(code.strip() == ":nread"):
            confirm = yes_no_dialog(
                title='Confirm', text='Do you want to read a notebook?\nit will delete all changes unsaved!')
            if(confirm):
                file_location=input_dialog("read notebook","Where is your notebook?\nexample:./a.tnote",)
                if(file_location):
                    try:
                        import pickle
                        import os
                        if(os.path.exists(file_location)):
                            with open(file_location,"rb",)as fp:
                                obj=pickle.load(fp,fix_imports=True)
                            his_str=obj["history"]
                            session.history=InMemoryHistory()
                            print(("notebook: "+file_location).center(40,"-"))
                            for i in his_str:
                                session.history.append_string(i)
                                lines=i.splitlines()
                                for j in range(len(lines)):
                                    if(j==0):
                                        print(f"{c.Fore.BLUE}>>>{c.Fore.RESET}",lines[j])
                                    else:
                                        print(f"...",lines[j])
                            for a in obj['locals']:
                                name,pi=a
                                if(__match_save_type__(pi) and type(pi)!=str):
                                    exec(f"{name}={pi}")
                                elif(type(pi)==str):
                                    exec(f"{name}='{pi}'")
                            if(obj["module"]): # 往前兼容
                                for a in obj["module"]:
                                    exec(f"import {a}")
                        else:
                            print(file_location,"not exists")
                    except Exception as e:
                        print(e)
                else:
                    print("canceled")
            else:
                print("canceled")
        else:
            __run_code__(code)
